﻿using Microsoft.Data.SqlClient;

namespace Movie_Characters_API.Models
{
    public class ConnectionStringHelper
    {

        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new();
            builder.DataSource = @"(localdb)\MSSQLLocalDB";
            builder.InitialCatalog = "MovieCharacterDb";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;
            return builder.ConnectionString;
        }

    }
}

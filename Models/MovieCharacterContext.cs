﻿namespace Movie_Characters_API.Models
{
    public class MovieCharacterContext: DbContext
    {
        public MovieCharacterContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Movie> Movie { get; set; }
        public virtual DbSet<Character> Character { get; set; }
        public virtual DbSet<Franchise> Franchise { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionStringHelper.GetConnectionString());
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 1,
                    MovieTitle = "Harry Potter and the Philosopher's Stone",
                    Director = "Chris Columbus",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg/220px-Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg",
                    Genre = "Fantasy",
                    ReleaseYear = "2001",
                    FranchiseId = 1,
                    Trailer = @"https://www.youtube.com/watch?v=wrMb0o6hlDQ"
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 2,
                    MovieTitle = "Harry Potter and the Chamber of Secrets",
                    Director = "Chris Columbus",
                    Picture = @"https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg",
                    Genre = "Fantasy",
                    ReleaseYear = "2002",
                    FranchiseId = 1,
                    Trailer = @"https://www.imdb.com/video/vi1705771289/?playlistId=tt0295297&ref_=tt_ov_vi"
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 3,
                    MovieTitle = "Harry Potter and the Prisoner of Azkaban",
                    Director = "Alfonso Cuarón",
                    Picture = @"https://static.posters.cz/image/1300/art-photo/harry-potter-fangen-fran-azkaban-i105075.jpg",
                    Genre = "Fantasy",
                    ReleaseYear = "2004",
                    FranchiseId = 1,
                    Trailer = @"https://www.imdb.com/video/vi2007761177/?playlistId=tt0304141&ref_=tt_ov_vi"
                });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 4,
                    MovieTitle = "Lord of the Rings: The Fellowship of the ring",
                    Director = "Peter Jackson",
                    Picture = @"https://www.looper.com/img/gallery/things-in-lord-of-the-rings-you-only-notice-as-an-adult/intro-1508529851.jpg",
                    Genre = "Adventure",
                    ReleaseYear = "2001",
                    FranchiseId = 2,
                    Trailer = @"https://www.imdb.com/video/vi684573465/?playlistId=tt0120737&ref_=tt_ov_vi"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id=1,
                    FullName="Harry Potter",
                    Alias="Potter",
                    Gender="Male",
                    Image= @"https://static.wikia.nocookie.net/harrypotter/images/c/cf/Harry-Potter-and-the-Deathly-Hallows-Part-2-2011-harry-potter-and-the-deathly-hallows-23712910-1200-800.jpg/revision/latest/top-crop/width/360/height/360?cb=20111228001303&path-prefix=sv"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 2,
                    FullName = "Hermoine Granger",
                    Gender = "Female",
                    Image = @"https://nouwcdn.com/14/1425000/1410000/1404898/pics/201705281552536128_sbig.jpg?version=202007&width=352"
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character
                {
                    Id = 3,
                    FullName = "Frodo B",
                    Gender = "Male",
                    Image = @"https://static.wikia.nocookie.net/harrypotter/images/c/cf/Harry-Potter-and-the-Deathly-Hallows-Part-2-2011-harry-potter-and-the-deathly-hallows-23712910-1200-800.jpg/revision/latest/top-crop/width/360/height/360?cb=20111228001303&path-prefix=sv"
                });
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id=1,
                    Name="Harry Potter",
                    Description="Story about Harry Potter."
                });
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise
                {
                    Id = 2,
                    Name = "Lord of The Rings",
                    Description = "Story about a ring."
                });
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 2 },
                            new { MovieId = 4, CharacterId = 3 }
                        );
                    });
        }


    }
}

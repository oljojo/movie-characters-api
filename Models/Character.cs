﻿namespace Movie_Characters_API.Models
{
    public class Character
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string? FullName { get; set; }
        [StringLength(100)]
        public string? Alias {get; set;}
        [StringLength(20)]
        public string? Gender { get; set;}
        [StringLength(1000)]
        public string? Image { get; set;}
        public ICollection<Movie>? Movies { get; set; }
    }
}

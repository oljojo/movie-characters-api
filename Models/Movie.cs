﻿namespace Movie_Characters_API.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string? MovieTitle { get; set; }
        [StringLength(100)]
        public string? Genre { get; set; }
        [StringLength(4)]
        public string? ReleaseYear { get; set; }
        [StringLength(100)]
        public string? Director { get; set; }
        [StringLength(1000)]
        public string? Picture { get; set; }
        [StringLength(1000)]
        public string? Trailer { get; set; }
        public ICollection<Character>? Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
    }
}

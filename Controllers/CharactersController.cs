﻿namespace Movie_Characters_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the characters.
        /// </summary>
        /// <returns>A list of characters or a status code.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacter()
        {
            if (_context.Character == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Character.Include(c => c.Movies).ToListAsync());
        }

        /// <summary>
        /// Gets a specific character by the given id.
        /// </summary>
        /// <param name="id">The id of the character.</param>
        /// <returns>A character or a status code.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            if (_context.Character == null)
            {
                return NotFound();
            }
            var character = await _context.Character.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Edits a character with the new data provided in the body.
        /// </summary>
        /// <param name="id">The id for the character.</param>
        /// <param name="character">The character.</param>
        /// <returns>A status code.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCharacterById(int id, CharacterEditDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            Character updatedCharacter = _context.Character.Include(m => m.Movies).Single(c => c.Id == character.Id);
            List<Movie> movies = new();
            foreach (int movieId in character.Movies)
            {
                Movie movie = await _context.Movie.FindAsync(movieId);
                if (movie == null) return BadRequest();
                movies.Add(movie);
            }
            _mapper.Map<CharacterEditDTO, Character>(character, updatedCharacter);

            updatedCharacter.Movies = movies;


            _context.Character.Update(updatedCharacter);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new character with the data provided in the body.
        /// </summary>
        /// <param name="character">The character data.</param>
        /// <returns>A status code.</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> CreateCharacter(CharacterCreateDTO character)
        {
            if (_context.Character == null)
            {
                return Problem("Entity set 'MovieCharacterContext.Character'  is null.");
            }
            Character domainCharacter = _mapper.Map<Character>(character);
            domainCharacter.Movies = (from m in _context.Movie where character.Movies.Contains(m.Id) select m).ToList();
            await _context.Character.AddAsync(domainCharacter);
            //await _context.Character.Include(c => c.Movies).ToList().AddAsync(domainCharacter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacterById", new { id = domainCharacter.Id }, character);
        }

        /// <summary>
        /// Deletes a character by the given id.
        /// </summary>
        /// <param name="id">The id of the character.</param>
        /// <returns>A status code.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacterById(int id)
        {
            if (_context.Character == null)
            {
                return NotFound();
            }
            var character = await _context.Character.Include(c => c.Movies).Select(c => c).Where(c => c.Id == id).SingleAsync();
            if (character == null)
            {
                return NotFound();
            }

            _context.Character.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return (_context.Character?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}

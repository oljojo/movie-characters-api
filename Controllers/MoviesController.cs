﻿namespace Movie_Characters_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all movies.
        /// </summary>
        /// <returns>A list of all movies or a status code.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            if (_context.Movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<MovieReadDTO>>(await _context.Movie.Include(m => m.Characters).ToListAsync());
        }

        /// <summary>
        /// Gets a specific movie by the given id.
        /// </summary>
        /// <param name="id">The id for the movie.</param>
        /// <returns>A movie or a status code.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieById(int id)
        {
            if (_context.Movie == null)
            {
                return NotFound();
            }
            var movie = await _context.Movie.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Gets all characters in a specific movie.
        /// </summary>
        /// <param name="id">The movie Id.</param>
        /// <returns>Returns a response body and status code.</returns>
        [HttpGet("GetCharacters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            if (_context.Movie == null)
            {
                return NotFound();
            }
            var movie = await _context.Movie.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
            ICollection<Character> characters = movie.Characters;

            List<Character> newCharacterList = new();
            foreach (var character in characters)
            {
                newCharacterList.Add(character);
            }


            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(newCharacterList);
        }

        /// <summary>
        /// Edits a movie with the data specified in the body.
        /// </summary>
        /// <param name="id">The id of the movie.</param>
        /// <param name="movie">The movie.</param>
        /// <returns>A status code.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMovie(int id, MovieEditDTO movie)
        {
            if( id != movie.Id)
            {
                return BadRequest();
            }

            Movie domainMovie = await _context.Movie.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == movie.Id);
            List<Character> characters = new();

            foreach (var characterId in movie.Characters)
            {
                Character character = await _context.Character.FindAsync(characterId);
                
                if (character == null) return BadRequest();
                characters.Add(character);
            }

            _mapper.Map<MovieEditDTO, Movie>(movie, domainMovie);

            domainMovie.Characters = characters;

            _context.Entry(domainMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(movie.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new movie with the data provided in the body.
        /// </summary>
        /// <param name="movie">The movie data.</param>
        /// <returns>A status code.</returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> CreateMovie(MovieCreateDTO movie)
        {
            if (_context.Movie == null)
            {
                return Problem("Entity set 'MovieCharacterContext.Movie'  is null.");
            }
            Movie domainMovie = _mapper.Map<Movie>(movie);

            List<Character> characters = new();
            foreach (int charId in movie.Characters)
            {
                Character character = await _context.Character.FindAsync(charId);
                if (character == null)
                    return BadRequest("Character doesn't exist");
                characters.Add(character);
            }
            domainMovie.Characters = characters;

            await _context.Movie.AddAsync(domainMovie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovieById", new { id = domainMovie.Id }, movie);
        }

        /// <summary>
        /// Deletes a movie with the given id.
        /// </summary>
        /// <param name="id">The id of the movie.</param>
        /// <returns>A status code.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovieById(int id)
        {
            if (_context.Movie == null)
            {
                return NotFound();
            }
            var movie = await _context.Movie.Include(m => m.Characters).Select(m => m).Where(m => m.Id == id).SingleAsync();
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Updates the characters in a movie by MovieId
        /// </summary>
        /// <param name="id">The id for the movie.</param>
        /// <param name="characterIds">The character IDs.</param>
        /// <returns>204 on success</returns>
        [HttpPut("UpdateCharacter/{id}")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, List<int> characterIds)
        {

            Movie toUpdateMovie = _context.Movie.Include(m => m.Characters).Single(m => m.Id == id);

            List<Character> characters = new();
            foreach (int charId in characterIds)
            {
                Character character = await _context.Character.FindAsync(charId);
                if (character == null)
                    return BadRequest("Movie doesn't exist");
                characters.Add(character);
            }

            toUpdateMovie.Characters = characters;

            _context.Entry(toUpdateMovie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Checks if the movie exists.
        /// </summary>
        /// <param name="id">The id for the movie.</param>
        /// <returns>True if the movie does exist, false if the movie does not exist.</returns>
        private bool MovieExists(int id)
        {
            return (_context.Movie?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}

﻿namespace Movie_Characters_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all franchises.
        /// </summary>
        /// <returns>A <see cref="Franchise"/> in JSON format or a <seealso cref="ActionResult"/> with a status code.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            if (_context.Franchise == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.Franchise.Include(f=>f.Movies).ToListAsync());
        }

        /// <summary>
        /// Gets a franchise by the given Id.
        /// </summary>
        /// <param name="id">The franchise Id.</param>
        /// <returns>A <see cref="Franchise"/> in JSON format or a <seealso cref="ActionResult"/> with a status code.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {
            if (_context.Franchise == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchise.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Edits a franchise with the new data provided in the body.
        /// </summary>
        /// <param name="id">The id of the franchise.</param>
        /// <param name="franchise">The franchise that should be edited.</param>
        /// <returns>A status code.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, FranchiseEditDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            Franchise toUpdateFranchise = _context.Franchise.Include(f => f.Movies).Single(f => f.Id == franchise.Id);

            List<Movie> movies = new();
            foreach (int movId in franchise.Movies)
            {
                Movie movie = await _context.Movie.FindAsync(movId);
                if (movie == null)
                    return BadRequest("Character doesn't exist");
                movies.Add(movie);
            }

            _mapper.Map<FranchiseEditDTO, Franchise>(franchise, toUpdateFranchise);

            toUpdateFranchise.Movies = movies;


            _context.Entry(toUpdateFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new franchise with the data provided in the body.
        /// </summary>
        /// <param name="franchise">The franchise that should be added.</param>
        /// <returns>A status code.</returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> CreateFranchise(FranchiseCreateDTO franchise)
        {
            if (_context.Franchise == null)
            {
                return Problem("Entity set 'MovieCharacterContext.Franchise'  is null.");
            }

            List<Movie> movies = new();
            foreach (int movId in franchise.Movies)
            {
                Movie movie = await _context.Movie.FindAsync(movId);
                if (movie == null)
                    return BadRequest("Franchise doesn't exist");
                movies.Add(movie);
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            domainFranchise.Movies = movies;

            _context.Franchise.Add(domainFranchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchiseById", new { id = domainFranchise.Id }, franchise);
        }

        /// <summary>
        /// Deletes a franchise by the given id.
        /// </summary>
        /// <param name="id">The id for the franchise.</param>
        /// <returns>A status code.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchiseById(int id)
        {
            if (_context.Franchise == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchise.Include(f => f.Movies).Select(f => f).Where(f => f.Id == id).SingleAsync();
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Gets a list of characters that is in a specific franchise
        /// </summary>
        /// <param name="id">The id for the franchise.</param>
        /// <returns>A list of characters on success or "Not found" if franchies doesn't exist</returns>
        [HttpGet("GetCharacters/{id}")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacterInFranchise(int id)
        {
            if (_context.Franchise == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchise.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            ICollection<Movie> movies = franchise.Movies;
            List<Character> characters = new List<Character>();
            foreach(Movie movie in movies)
            {
                Movie tempMovie = await _context.Movie.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == movie.Id);
                foreach(Character character in tempMovie.Characters)
                {
                    if(!characters.Contains(character))
                        characters.Add(character);
                }
            }

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(characters);
        }
        /// <summary>
        /// Gets a list of movies in a franchise based on the id of the franchise.
        /// </summary>
        /// <param name="id">The id for the franchise.</param>
        /// <returns>A list of movies. "Not Found" if the franchise doesn't exist.</returns>
        [HttpGet("GetMovies/{id}")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            if (_context.Franchise == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchise.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            ICollection<Movie> movies = franchise.Movies;

            List<Movie> newMovieList = new();
            foreach(Movie movie in movies)
            {
                newMovieList.Add(movie);
            }


            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieReadDTO>>(newMovieList);
        }
        /// <summary>
        /// Update the movies that is connected to a franchise.
        /// </summary>
        /// <param name="id">The id for the franchise.</param>
        /// <param name="movieIds">The movie IDs.</param>
        /// <returns>No content on Success. "Not Found" if the movies doesn't exist.</returns>
        [HttpPut("UpdateMovies/{id}")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, List<int> movieIds)
        {

            Franchise toUpdateFranchise = _context.Franchise.Include(f => f.Movies).Single(f => f.Id == id);

            List<Movie> movies = new();
            foreach (int movId in movieIds)
            {
                Movie movie = await _context.Movie.FindAsync(movId);
                if (movie == null)
                    return BadRequest("Movie doesn't exist");
                movies.Add(movie);
            }

            toUpdateFranchise.Movies = movies;

            _context.Entry(toUpdateFranchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Checks if the franchise does exist.
        /// </summary>
        /// <param name="id">The id for the franchise.</param>
        /// <returns>true if the franchise does exist, false if the franchise does not exist.</returns>
        private bool FranchiseExists(int id)
        {
            return (_context.Franchise?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}

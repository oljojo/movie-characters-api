﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Movie_Characters_API.Migrations
{
    public partial class inserts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CharacterMovie_Character_CharactersId",
                table: "CharacterMovie");

            migrationBuilder.DropForeignKey(
                name: "FK_CharacterMovie_Movie_MoviesId",
                table: "CharacterMovie");

            migrationBuilder.RenameColumn(
                name: "MoviesId",
                table: "CharacterMovie",
                newName: "CharacterId");

            migrationBuilder.RenameColumn(
                name: "CharactersId",
                table: "CharacterMovie",
                newName: "MovieId");

            migrationBuilder.RenameIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie",
                newName: "IX_CharacterMovie_CharacterId");

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Image" },
                values: new object[,]
                {
                    { 1, "Potter", "Harry Potter", "Male", "https://static.wikia.nocookie.net/harrypotter/images/c/cf/Harry-Potter-and-the-Deathly-Hallows-Part-2-2011-harry-potter-and-the-deathly-hallows-23712910-1200-800.jpg/revision/latest/top-crop/width/360/height/360?cb=20111228001303&path-prefix=sv" },
                    { 2, null, "Hermoine Granger", "Female", "https://nouwcdn.com/14/1425000/1410000/1404898/pics/201705281552536128_sbig.jpg?version=202007&width=352" },
                    { 3, null, "Frodo B", "Male", "https://static.wikia.nocookie.net/harrypotter/images/c/cf/Harry-Potter-and-the-Deathly-Hallows-Part-2-2011-harry-potter-and-the-deathly-hallows-23712910-1200-800.jpg/revision/latest/top-crop/width/360/height/360?cb=20111228001303&path-prefix=sv" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Story about Harry Potter.", "Harry Potter" },
                    { 2, "Story about a ring.", "Lord of The Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "Chris Columbus", 1, "Fantasy", "Harry Potter and the Philosopher's Stone", "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg/220px-Harry_Potter_and_the_Philosopher%27s_Stone_banner.jpg", "2001", "https://www.youtube.com/watch?v=wrMb0o6hlDQ" },
                    { 2, "Chris Columbus", 1, "Fantasy", "Harry Potter and the Chamber of Secrets", "https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg", "2002", "https://www.imdb.com/video/vi1705771289/?playlistId=tt0295297&ref_=tt_ov_vi" },
                    { 3, "Alfonso Cuarón", 1, "Fantasy", "Harry Potter and the Prisoner of Azkaban", "https://static.posters.cz/image/1300/art-photo/harry-potter-fangen-fran-azkaban-i105075.jpg", "2004", "https://www.imdb.com/video/vi2007761177/?playlistId=tt0304141&ref_=tt_ov_vi" },
                    { 4, "Peter Jackson", 2, "Adventure", "Lord of the Rings: The Fellowship of the ring", "https://www.looper.com/img/gallery/things-in-lord-of-the-rings-you-only-notice-as-an-adult/intro-1508529851.jpg", "2001", "https://www.imdb.com/video/vi684573465/?playlistId=tt0120737&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 4 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_CharacterMovie_Character_CharacterId",
                table: "CharacterMovie",
                column: "CharacterId",
                principalTable: "Character",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CharacterMovie_Movie_MovieId",
                table: "CharacterMovie",
                column: "MovieId",
                principalTable: "Movie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CharacterMovie_Character_CharacterId",
                table: "CharacterMovie");

            migrationBuilder.DropForeignKey(
                name: "FK_CharacterMovie_Movie_MovieId",
                table: "CharacterMovie");

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.RenameColumn(
                name: "CharacterId",
                table: "CharacterMovie",
                newName: "MoviesId");

            migrationBuilder.RenameColumn(
                name: "MovieId",
                table: "CharacterMovie",
                newName: "CharactersId");

            migrationBuilder.RenameIndex(
                name: "IX_CharacterMovie_CharacterId",
                table: "CharacterMovie",
                newName: "IX_CharacterMovie_MoviesId");

            migrationBuilder.AddForeignKey(
                name: "FK_CharacterMovie_Character_CharactersId",
                table: "CharacterMovie",
                column: "CharactersId",
                principalTable: "Character",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CharacterMovie_Movie_MoviesId",
                table: "CharacterMovie",
                column: "MoviesId",
                principalTable: "Movie",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

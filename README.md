# Assignment 6 - Movie Characters API

The project is an basic Web API for Movies, their Franchises and Characters in the movies. Through the API movies, characters and franchises can be displayed, created and updated. There are also some special endpoints to display connections between these aswell as update the connections.

## Table of Contents

- [Clone](#clone)
- [Install](#install)
- [Usage](#usage)
- [Start](#start)
- [Contributors](#contributors)

## Clone

You can clone this repository for your own uses through the following command in your terminal.
```
$cd (Chosen folder)
$git clone https://gitlab.com/oljojo/movie-characters-api.git
```

## Install

After downloading the code, open the solution (Movie-Characters-API.sln) with Visual Studio.

To seed the database with the seed-data created the following command must be executed in the "Package Manager Console" of Visual Studio 
```
$update-database
```
## Usage

The software will start a Web API server with Swagger included to easily run the requests that can be executed. The paths are separated based on Movie, Character and Franchise, all with separate endpoints for GetAll, GetById, Create, UpdateById and Delete. 

Franchise and Move also have some special endpoints. Movie has an endpoint that makes it possible to change the character of that movie based on the id and an endpoint to display only all the characters that is in that movie, based on its id. Franchise has three special endpoints, one that displays the movies in a franchise, one that displays the character that is in a franchise and one that can update the movies that are in a franchise.

## Start

Once the solution is opened in Visual Studio it can easily be run through Visual studio. A server should be started and the browser be opened automatically.

## Contributors

Jeremy - @Jerry585

Olof - @oljojo 

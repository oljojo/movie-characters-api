﻿namespace Movie_Characters_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToList()))
                .ReverseMap();
            CreateMap<FranchiseCreateDTO, Franchise>()
                .ForMember(f => f.Movies, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<FranchiseEditDTO, Franchise>()
                .ForMember(f => f.Movies, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}

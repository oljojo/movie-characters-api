﻿namespace Movie_Characters_API.Profiles
{
    public class MovieProfile : Profile
        
    {
        public MovieProfile()
        {


            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.FranchiseId, opt => opt
                .MapFrom(a => a.FranchiseId))
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Id).ToList()))
                .ReverseMap();
            CreateMap<MovieCreateDTO, Movie>()
                .ForMember(m => m.FranchiseId, opt => opt
                .MapFrom(mdto => mdto.FranchiseId))
                .ForMember(m => m.Characters, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<MovieEditDTO, Movie>()
                .ForMember(m => m.FranchiseId, opt => opt
                .MapFrom(mdto => mdto.FranchiseId))
                .ForMember(m => m.Characters, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}

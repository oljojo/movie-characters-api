﻿namespace Movie_Characters_API.Profiles
{
    public class CharacterProfile : Profile
    {
        private readonly MovieCharacterContext _context;
        public CharacterProfile(MovieCharacterContext context)
        {
            _context = context;
        }
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt=>opt
                .MapFrom(c=>c.Movies.Select(m=>m.Id).ToList()))
                .ReverseMap();
            CreateMap<CharacterCreateDTO, Character>()
                .ForMember(c => c.Movies, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<CharacterEditDTO, Character>()
                .ForMember(c => c.Movies, opt => opt.Ignore())
                .ReverseMap();
        }
        
    }
}

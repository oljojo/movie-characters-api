﻿namespace Movie_Characters_API.DTOs.FranchiseDTOs
{
    public class FranchiseReadDTO
    {
        int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public List<int>? Movies { get; set; }
    }
}

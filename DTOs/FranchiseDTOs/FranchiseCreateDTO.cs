﻿namespace Movie_Characters_API.DTOs.FranchiseDTOs
{
    public class FranchiseCreateDTO
    {
        [StringLength(50)]
        public string? Name { get; set; }
        [StringLength(100)]
        public string? Description { get; set; }
        public List<int>? Movies { get; set; }
    }
}

﻿namespace Movie_Characters_API.DTOs.CharacterDTOs
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string? Image { get; set; }
        public List<int>? Movies { get; set; }
    }
}

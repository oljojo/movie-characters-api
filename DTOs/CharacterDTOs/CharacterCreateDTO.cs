﻿namespace Movie_Characters_API.DTOs.CharacterDTOs
{
    public class CharacterCreateDTO
    {
     
        [StringLength(100)]
        public string? FullName { get; set; }
        [StringLength(100)]
        public string? Alias { get; set; }
        [StringLength(20)]
        public string? Gender { get; set; }
        [StringLength(1000)]
        public string? Image { get; set; }
        public List<int>? Movies { get; set; }
    }
}
